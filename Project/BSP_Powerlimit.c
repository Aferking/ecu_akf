#include "appl.h"
#include "PID_Controller.h"
#include "BSP_Powerlimit.h"

static float integral_max = 0; //cant have the PID give additional torque
static float integral_min = -32767; //PID couldt potentially reduce torque to zero
static float pgain = 1;
static float igain = 0;
static float dgain = 0; 

static float prev_error = 0; //initialize 
static float integral_error = 0; //initialize

float power_error = 0;

float power_setpoint = 0;

float powerlimit(float current_sens, float voltage_sens)
{
	PID powerlimit;
	
	powerlimit.integrator_max = integral_max;
	powerlimit.integrator_min = integral_min;
	powerlimit.integral_error = integral_error;
	powerlimit.prev_error = prev_error;
	powerlimit.pGain = pgain;
	powerlimit.iGain = igain;
	powerlimit.dGain = dgain;
	
	power_error = power_setpoint - (current_sens * voltage_sens);
	
	return updatePID(&powerlimit, power_error);
}
