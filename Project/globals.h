#include "stdint.h"

/* VALUES */
/* PEDALBOX VALUES */
extern uint16_t G_throttleValue;
extern uint16_t G_brakeValue;
/* BMS VALUES */
extern uint16_t G_cellMinVolt;
extern uint16_t G_cellMaxVolt;
extern uint16_t G_cellMaxTemp;
extern uint16_t G_cellMinTemp;
extern uint16_t G_battVolt;
extern uint16_t G_battSoc;
extern uint16_t G_BmsError;

/* Motorcontroller */
extern uint16_t G_tempMC;
extern uint16_t G_tempMotor;

/* FLAGS */
extern uint8_t F_msgBms;
extern uint8_t F_msgMc;
extern uint8_t F_newThrottle;
extern uint8_t F_msgBrake;

/* BMS */
extern uint8_t F_cellMinVolt;
extern uint8_t F_cellMaxVolt;
extern uint8_t F_battVolt;
extern uint8_t F_cellMinTemp; 
extern uint8_t F_cellMaxTemp; 
extern uint8_t F_battSoc;     
extern uint8_t F_BmsError;   
/* Motorcontroller */
