/* Includes */
#include "appl.h"
#include "PID_Controller.h"
#include "BSP_Traction_control.h"
#include "BSP_Powerlimit.h"
#include "globals.h"
#include "BSP_GPIO.h"
#include "elithion.h"
#include "controller.h"

/* config */
#define drivemode /* Safety measures will be bypassed if not defined! */

/*Function prototypes*/
static void startup(void);

/*Variables*/

int main(void)
{
    BSP_systick_init();
    BSP_GPIO_init();
	BSP_LED_init(); 
    BSP_CAN_init();
    BSP_CAN2_init();
    
    /* TODO : Check if a timeout has occoured. */
    startup(); 
    while(1)
	{
        /* TODO : Any errors present? */
        /* TODO : Is the contactors still closed?*/
        /* TODO : Does all the modules respond ? */
        /* Check BMS data */
        elithion_process();
        /* TODO : check motorcontroller data */
        /* Calculate and request new throttle value */
        controller_process();
        /* TODO : tick watchdog */
    }
}

/**
    @brief  a check to see if the car is good to drive. If any tests fail
            the car won't start and an error message gets transmittet over
            the can bus to tell the driver whats wrong.
*/
static void startup(void)
{
    #ifdef drivemode 
    /* TODO : Any errors presents ? */
    /* TODO : Does all the modules respond ? 
                SM:PB, SM:HVB, SM:BB, BAMOCAR, BMS, DB*/
    /* TODO : Are the contactors welded ? */
    
    /* Close the pilot line! */
    BSP_GPIO_pilotline(ON);
    /* TODO : wait for precharging to be done. */
    /* TODO : Are the contactors closed ? */
    
    /* TODO : Wait for start signal from driver */
    /* TODO : Transmit startup successfull if nothing is wrong */
    /* Start the fans */
    BSP_GPIO_FANS(ON);
    /* Play startup noise!*/ 
    BSP_GPIO_RTDS();
    
    #endif
    
    /* Arm the motor! */
    BSP_GPIO_motor(ON);
    /* TODO : tick watchdog */
}
