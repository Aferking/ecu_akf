#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "BSP_GPIO.h"
#include "BSP_Systick.h"

/* Local Function prototypes */

/* MACROS */
#define GPIO_HIGH(ch,pin) (ch->BSRRL |= pin)
#define GPIO_LOW(ch,pin) (ch->BSRRH |= pin)

/* Constants for GPIOs that are used on the ECU */
#define PIN_RTDS GPIO_Pin_1
#define GPIO_RTDS GPIOC

#define PIN_PILOTLINE GPIO_Pin_0
#define GPIO_PILOTLINE GPIOC

#define PIN_BAMOCAR_RUN_SIGNAL GPIO_Pin_2
#define GPIO_BAMOCAR_RUN_SIGNAL GPIOC

#define PIN_BRAKELIGHT GPIO_Pin_7
#define GPIO_BRAKELIGHT GPIOB

#define PIN_FAN_RADIATOR GPIO_Pin_12
#define GPIO_FAN_RADIATOR GPIOB

#define PIN_FAN_BACKPLATE GPIO_Pin_13
#define GPIO_FAN_BACKPLATE GPIOB

#define PIN_CAN1_EN GPIO_Pin_12
#define GPIO_CAN1_EN GPIOA

#define PIN_CAN2_EN GPIO_Pin_11
#define GPIO_CAN2_EN GPIOA

#define LED1 GPIO_Pin_4
#define LED2 GPIO_Pin_5
#define LED3 GPIO_Pin_6
#define LED4 GPIO_Pin_7
#define GPIO_LED GPIOA

#define GPIOA_PINS LED1|LED2|LED3|LED4
#define GPIOB_PINS PIN_FAN_BACKPLATE|PIN_FAN_RADIATOR|PIN_BRAKELIGHT
#define GPIOC_PINS PIN_BAMOCAR_RUN_SIGNAL|PIN_PILOTLINE|PIN_RTDS

void BSP_GPIO_init()
{
    GPIO_InitTypeDef GPIO_initStruct;
    /* Enable clocks to the used periphs */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
   
    /* Configure the PP outputs for channel A */    
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType  = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed  = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin    = GPIOA_PINS;
    GPIO_Init(GPIOA, &GPIO_initStruct);
    
    /*Configure the OD pull-ups for channel A*/    
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType  = GPIO_OType_OD;
	GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_UP;
	GPIO_initStruct.GPIO_Speed  = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin    = PIN_CAN1_EN | PIN_CAN2_EN;
    GPIO_Init(GPIOA, &GPIO_initStruct);
    
    /* Configure the PP outputs for channel B */    
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType  = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed  = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin    = GPIOB_PINS;
    GPIO_Init(GPIOB, &GPIO_initStruct);
    
    /* Configure the PP outputs for channel C */    
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType  = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed  = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin    = GPIOC_PINS;
    GPIO_Init(GPIOC, &GPIO_initStruct);
}

void BSP_GPIO_FANS(uint8_t state)
{
    GPIO_WriteBit(GPIO_FAN_BACKPLATE, PIN_FAN_BACKPLATE, state);
    GPIO_WriteBit(GPIO_FAN_RADIATOR, PIN_FAN_RADIATOR, state);
}


void BSP_GPIO_motor(uint8_t state)
{
    GPIO_WriteBit(GPIO_BAMOCAR_RUN_SIGNAL, PIN_BAMOCAR_RUN_SIGNAL, state);
}

void BSP_GPIO_pilotline(uint8_t state)
{
    GPIO_WriteBit(GPIO_PILOTLINE, PIN_PILOTLINE, state);
}
void BSP_GPIO_RTDS(void)
{
    /*Turn on RTDS*/
    GPIO_WriteBit(GPIO_BAMOCAR_RUN_SIGNAL, PIN_BAMOCAR_RUN_SIGNAL, ON);
    /* Delay 2 sec */
    clk2000ms = CLK_RESET;
    while(clk2000ms != CLK_COMPLETE)
    {
    };
    /* Turn off RTDS */
    GPIO_WriteBit(GPIO_BAMOCAR_RUN_SIGNAL, PIN_BAMOCAR_RUN_SIGNAL, OFF);
}

void BSP_GPIO_brakelight(uint8_t state)
{
    GPIO_WriteBit(GPIO_BRAKELIGHT, PIN_BRAKELIGHT, state);
}
/* LOCAL FUNCITONS ---------------------------------------------*/

