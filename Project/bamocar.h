/* Constants */
#define BAMOCAR_ID_REQ_RPM 0x31     /* TX ID used for requesting a rpm */
#define BAMOCAR_ID_REQ_TORQUE 0x90  /* TX ID used for requesting a torque */
#define BAMOCAR_ID_REQ_CAL          /* TX ID used for requesting motor calibration */
#define BAMOCAR_ID_REQ_DATA         /* TX ID used for requesting data */

void bamocar_Tx(int16_t throttle);
