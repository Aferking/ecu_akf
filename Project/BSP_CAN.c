#include "BSP_CAN.h"
#include "stdint.h"
#include "stm32f4xx_can.h"
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "BSP_USART.h"
#include "BSP_systick.h"
#include "BSP_Interrupt.h"
#include "CAN_ID_LIST.h"
#include "BSP_Traction_control.h"
#include "appl.h"
#include "globals.h"
/* 
* ECU board
* PB5 : CAN2 RX
* PB6 : CAN2 TX
*/
GPIO_InitTypeDef  GPIO_InitStructure;
CAN_InitTypeDef CAN_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;
CAN_FilterInitTypeDef CAN_FilterInitStructure;

void BSP_CAN_init(void){ // CAN Setup
	/* Have to set the can tranceiver EN pin high to enable it */
	GPIO_SetBits(GPIOA, GPIO_Pin_12); //CAN1 Enable on ECU board
    /* Enable the clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;  //CAN RX: PB8,     CAN TX: PB9
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//Alternate Function for the GPIOB pins 8 & 9. 
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1);
	
	//CAN1 enable setup
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

	/* CAN1 register init */
	CAN_DeInit(CAN1);

	/* CAN1 cell init */
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_3tq;                    

	/* CAN1 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/        //30MHz 1MBps
	CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;                    //6tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                    //8tq;
	CAN_InitStructure.CAN_Prescaler =(42000000 / 7) / 500000;                    //
	CAN_Init(CAN1, &CAN_InitStructure);

	/* CAN1 reset */
	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
    
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

uint8_t BSP_CAN_Tx(uint32_t address, uint8_t length, uint8_t data[8]) 
{	
    CanTxMsg txmsg;
    uint8_t i=0;
    
	txmsg.StdId = address;
	txmsg.IDE = CAN_Id_Standard;
	txmsg.RTR = CAN_RTR_Data;
	txmsg.DLC = length;
		
	for(i=0; i<length;i++){
		txmsg.Data[i] = data[i];
	}
	return CAN_Transmit(CAN1, &txmsg);
}

void BSP_CAN2_init(void){ // CAN Setup
	/* Have to set the can tranceiver EN pin high to enable it */
	GPIO_SetBits(GPIOA, GPIO_Pin_11); //CAN2 Enable on ECU board

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;  //CAN2 RX: PB5,     CAN2 TX: PB6
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//Alternate Function for the GPIOB pins 5 & 6. 
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_CAN2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_CAN2);
	
	//CAN2 enable setup
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);
    
	/* CAN2 reset */
	CAN_DeInit(CAN2);

	/* CAN2 cell init */
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_3tq;                    //1tq;

	/* CAN2 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/        //30MHz 1MBps
	CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;                    //6tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                    //8tq;
	CAN_InitStructure.CAN_Prescaler =(42000000 / 7) / 500000;                    //
	CAN_Init(CAN2, &CAN_InitStructure);

	/* CAN2 filter init */
	CAN_FilterInitStructure.CAN_FilterNumber = 14; //filter number for CAN1 must be 0..13    and filter number for CAN2 must be 14..27
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CAN2, CAN_IT_FMP0, ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = CAN2_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

uint8_t BSP_CAN2_Tx(uint32_t address, uint8_t length, uint8_t data[8]) 
{
	CanTxMsg txmsg;
    uint8_t k = 0;
	txmsg.StdId = address;
	txmsg.IDE = CAN_Id_Standard;
	txmsg.RTR = CAN_RTR_Data;
	txmsg.DLC = length;
	
	for(k=0; k<length;k++){
		txmsg.Data[k] = data[k];
	}
	return CAN_Transmit(CAN2, &txmsg);
}

#define MASK_LOWER_BYTE(data) (uint8_t)(data & 0xFF)
#define MASK_UPPER_BYTE(data) (uint8_t)(data >> 8)

uint8_t BSP_CAN_TxHalfword(uint32_t ID, uint8_t length, uint16_t halfword[4])
{
    uint8_t k = 0;
    uint8_t n = 0;
    uint8_t data[8];
    
    for(k = 0; k<length; k++)
    {
        data[n] = MASK_LOWER_BYTE(halfword[k]);
        data[n+1] = MASK_UPPER_BYTE(halfword[k]);
        n+=2;
    }
    return BSP_CAN_Tx(ID, length, data); 
}

uint8_t BSP_CAN2_TxHalfword(uint32_t ID, uint8_t length, uint16_t halfword[4])
{
    uint8_t k = 0;
    uint8_t n = 0;
    uint8_t data[8];
    
    for(k = 0; k<length; k++)
    {
        data[n] = MASK_LOWER_BYTE(halfword[k]);
        data[n+1] = MASK_UPPER_BYTE(halfword[k]);
        n+=2;
    }
    return BSP_CAN2_Tx(ID, length, data); 
}
#define byte2halfword(byte1, byte2) ( (byte1 << 8) | (byte2 & 0xff ))

#define BMS_MSG 0x7E8

#define cellMinVoltID 0x43
#define cellMaxVoltID 0x45
#define battVoltID    0x46
#define cellMinTempID 0x47
#define cellMaxTempID 0x49
#define battSocID     0x50
#define BmsErrorID    0x62
#define THROTTLE_ID      0x0

uint16_t G_throttleValue = 0;
uint16_t G_brakeValue = 0;
uint16_t G_cellMinVolt = 0;
uint16_t G_cellMaxVolt = 0;
uint16_t G_cellMaxTemp = 0;
uint16_t G_cellMinTemp = 0;
uint16_t G_battVolt = 0;
uint16_t G_battSoc = 0;
uint16_t G_BmsError = 0;
uint16_t G_tempMC = 0;
uint16_t G_tempMotor = 0;
uint8_t F_msgBms = 0;
uint8_t F_msgMc = 0;
uint8_t F_newThrottle = 0;
uint8_t F_msgBrake = 0;
uint8_t F_cellMinVolt = 0;
uint8_t F_cellMaxVolt = 0;
uint8_t F_battVolt = 0;
uint8_t F_cellMinTemp = 0;
uint8_t F_cellMaxTemp = 0;
uint8_t F_battSoc     = 0;
uint8_t F_BmsError   = 0;
 

void CAN1_RX0_IRQHandler(void)
{		
	CanRxMsg rxmsg;
    
	__disable_irq();
	if(CAN1->RF0R & CAN_RF0R_FMP0) 
	{ 	
		CAN_Receive(CAN1, CAN_FIFO0, &rxmsg);   
        switch(rxmsg.StdId)
        {
            case BMS_MSG :
            switch(rxmsg.Data[2])
            {
                case cellMinVoltID :
                {
                    F_cellMinVolt = 1;
                    G_cellMinVolt = rxmsg.Data[4];
                    break;
                }
                case cellMaxVoltID :
                {
                    F_cellMaxVolt = 1;
                    G_cellMaxVolt = rxmsg.Data[4];
                    break;
                }
                case battVoltID :
                {
                    F_battVolt = 1;
                    G_cellMaxVolt = byte2halfword(rxmsg.Data[4],rxmsg.Data[5]);
                    break;
                }
                case cellMinTempID :
                {
                    F_cellMinTemp = 1;
                    G_cellMinTemp = rxmsg.Data[4];
                    break;
                }
                case cellMaxTempID :
                {
                    F_cellMaxTemp = 1;
                    G_cellMaxTemp = rxmsg.Data[4];
                    break;
                }
                case battSocID :
                {
                    F_battSoc = 1;
                    G_battSoc = rxmsg.Data[4];
                    break;
                }
                case BmsErrorID :
                {
                    F_BmsError = 1;
                    G_BmsError = rxmsg.Data[4];
                }
                
                case THROTTLE_ID : 
                {
                    F_newThrottle = 1;
                    G_throttleValue = byte2halfword(rxmsg.Data[0], rxmsg.Data[1]);
                }
            }
            
            break;
        }
	}
    __enable_irq();
}

#define MC_MSG  0x190
 uint8_t F_Rx_BMS_MSG = 0;

void CAN2_RX0_IRQHandler(void)
{
    if(CAN2->RF0R & CAN_RF0R_FMP0) 
    { 					
        CanRxMsg rxmsg;
		CAN_Receive(CAN2, CAN_FIFO0, &rxmsg);	
    }   
}		

