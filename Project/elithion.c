#include "stdint.h"
#include "BSP_CAN.h"
#include "globals.h"
#include "BSP_GPIO.h"

void elithion_process()
{
    if(F_cellMinVolt == 1)
    {
        F_cellMinVolt = 0;
    }
    else if(F_cellMaxVolt == 1)
    {
        F_cellMaxVolt = 0;
    }
    else if(F_battVolt == 1)
    {
    }
    else if(F_cellMinTemp == 1)
    {
        F_cellMinTemp = 0;
    }
    else if(F_cellMaxTemp == 1)
    {
        F_cellMaxTemp = 0;
    }
    else if(F_battSoc == 1)
    {
        F_battSoc = 0;
    }
    else if(F_BmsError == 1)
    {
        F_BmsError = 0;
        //transmitt
        BSP_GPIO_pilotline(OFF);
        
    }
}
