#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "globals.h"
#include "BSP_CAN.h"

#define direction -1;
#define RPM_MODE 0x31
//#define TORQUE_MODE 0x90
#define SCALING_FACTOR 0.5

#ifdef RPM_MODE
#define MODE RPM_MODE
#else
#define MODE TORQUE_MODE
#endif

void controller_process()
{
    uint16_t throttleValue = G_throttleValue;
    throttleValue = throttleValue * SCALING_FACTOR;
    if(F_newThrottle == 1)
    {
        F_newThrottle = 0;
        /* Motorcontroller needs integer values */
        throttleValue = (int)throttleValue;
        if(throttleValue < 200)
            {
                throttleValue = 0;
            }
        /* Set the correct direction on the motor */
        throttleValue = throttleValue * direction;
        BSP_CAN2_TxHalfword(MODE, 1, &throttleValue);
    }
}