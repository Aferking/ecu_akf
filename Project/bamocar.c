#include "stdint.h"
#include "BSP_CAN.h"
#include "config.h"
#include "BSP_GPIO.h"


#define RPM_MODE        /* Sets the car in RPM mode, only used on the test bench */
//#define TORQUE_MODE   /* Sets the car in Torque mode, used for driving. */
/* Direction */

#define direction -1    /* -1 for forward, 1 for backwards */



void bamocar_init(void)
{
    // Verify that stored settings are correct.
}
void bamocar_calibrateMotor(void)
{
}

uint16_t bamocar_reqTempMotor()
{
    return 0;
}

uint16_t bamocar_reqTempMC()
{
    return 0;
}

void arm_motor()
{
	//GPIO_SetBits(GPIOC, GPIO_Pin_2);
}

void disarm_motor()
{
	//GPIO_ResetBits(GPIOC, GPIO_Pin_2);
}
