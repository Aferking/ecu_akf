#include "BSP_USART.h"

/*midlertidig defines for at koden skal kompilere*/
#define RPI_DATA 0x000
#define CAN_RPI_DATA 0x001

// USART1 Tx: PA9
// USART1 Rx: PA10


//ringBuffer usart_rx_buffer;

void BSP_USART_init(void);
void BSP_USART_puts(volatile char *s);
void USART1_IRQHandler(void);
void USB_LP_CAN1_RX0_IRQHandler(void);
void BSP_RPI_Send(uint8_t address, uint8_t data1, uint8_t data2);
void UART_SendByte(char byte);

uint8_t calib_buffer[200]; //data from user to choose which sensor/module to be calibrated
uint8_t calib_request = 0;
uint8_t usart_id = 0;
uint8_t usart_data = 99;  //this is just "pseudocode" or how i imagine the interrupt will handle the usart data


uint8_t buffer[200];
uint8_t readingAddress = 0;
uint8_t state = 0;
uint8_t current = 0;



void BSP_USART_init(void){

	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;

	GPIO_Init(GPIOB, &GPIO_InitStruct);
		
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);
	
	USART_InitStruct.USART_BaudRate = 9600;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b; //8 bit data frame
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_Init(USART1, &USART_InitStruct);
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
		
	NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
}

void UART_SendByte(char byte){ //char is 1 byte (8 bit) long. -128 to 127 or 0 to 255
	USART_SendData(USART1, byte);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
}

void BSP_RPI_Send(uint8_t address, uint8_t data1, uint8_t data2){
	
	UART_SendByte(0xFF);
	UART_SendByte(0xFF);
	UART_SendByte(address);
	UART_SendByte(data2);
	UART_SendByte(data1);
}



void BSP_USART_Rx(uint8_t address, uint8_t data1, uint8_t data2){  // data received from the Raspberry Pi, sending it over CAN1 bus
	if(address == RPI_DATA) {
		uint8_t data[8];
		data[0] = data2;
		
		BSP_CAN_Tx(CAN_RPI_DATA, 1, data);
	}
	
	
}



//the USART1 on the RPI module will receive data from user (if calibrating is OK and such)
// 
void USART1_IRQHandler(void){
	static uint16_t RxByte = 0x00; 
	//ringBuffer_init(&usart_rx_buffer, 0);
	
//	if(USART_ReceiveData(USART1) == 'h') 	GPIO_ToggleBits(GPIOD, GPIO_Pin_14);
	
	/*
	
	if(USART_GetITStatus(USART1, USART_IT_RXNE)){
		
		uint8_t data = USART_ReceiveData(USART1);
		
		switch(state) {
			case 0:
				if(data == 0xFF) state++;
				break;
			
			case 1:
				if(data == 0xFF) state++;
				else state = 0;
				break;
			
			case 2:
				readingAddress = data;
				state++;
				break;
			
			case 3: 
				buffer[current++] = data;
				if(current >=2){
					state = 0;
					readingAddress = 0;
					current = 0;
					
					BSP_USART_Rx(readingAddress, buffer[1], buffer[0]);
				}
				break;
				
			default: 
				break;
		}
	}
	*/

	
	

	
	if (USART_GetITStatus(USART1, USART_IT_RXNE)) {
		 
		 if (USART_GetFlagStatus(USART1, USART_FLAG_RXNE)) { 
			 
			 /* Calibration part comm with raspberry pi  */
			 if(usart_id == CAL_REQUEST){
				 if(usart_data == 0) calib_request = 0;
				if(usart_data == 1) calib_request = 1;   //where "usart_data == 1" means that the data received over usart is in some form or other that the user wants to calibrate. 
			 }
			 
			 if(calib_request == 1){
				if(usart_id == CAL_THROTTLE){
			//		cal_throttle_data = USART_ReceiveData(USART1);
				}
				if(usart_id == CAL_BREAK) {
				//	cal_break_data = USART_ReceiveData(USART1);
				}
				//and so on
				
			 }
			 
			 
			 
			 
			 RxByte = USART_ReceiveData(USART1);
	//  	 ringBuffer_insert(&usart_rx_buffer, RxByte);
			
			 if(RxByte == 0xFF) calib_buffer[0] = RxByte;
			 
		//	 if(ringBuffer_read(&usart_rx_buffer) == 'h') GPIO_ToggleBits(GPIOD, GPIO_Pin_14);
			 if(RxByte== 'h') GPIO_ToggleBits(GPIOD, GPIO_Pin_14);

			 USART_ITConfig(USART1, USART_IT_TC, ENABLE);
		 }
		 
		 USART_ClearITPendingBit(USART1, USART_IT_RXNE);	
	 }
	 
/*
	 if (USART_GetITStatus(USART1, USART_IT_TC) == SET) {
	 
		 if (USART_GetFlagStatus(USART1, USART_FLAG_TC)) {
			 
	  	 USART_SendData(USART1, RxByte);
	// 		 USART_SendData(USART1, ringBuffer_read(&usart_rx_buffer));

			 USART_ITConfig(USART1, USART_IT_TC, DISABLE);
		 }
			 
		 USART_ClearITPendingBit(USART1, USART_IT_TC);
	 }
	 */
	 
	 
}

void BSP_USART_puts(volatile char *s){
		while(*s){
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);  // 
			USART_SendData(USART1, *s);
			*s++;
		}

	

}



