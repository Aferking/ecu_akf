#include "BSP_LED.h"
#include "BSP_systick.h"
#include "stm32f4xx_tim.h"
#include "BSP_Interrupt.h"
#include "BSP_USART.h"
#include "BSP_Startup.h"
#include "BSP_CAN.h"
#include "CAN_ID_LIST.h"
#include "BSP_PWM_FANS.h"

extern uint8_t motor_ARMED;
void RTDS(void);
