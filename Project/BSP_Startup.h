#include "stdint.h"
extern uint8_t calibrating_done;
extern uint8_t fault_check;
extern uint8_t precharge_done;
extern uint8_t startup_done;

void BSP_Startup_startup_routine(void);
void BSP_Startup_check_errors(void);
void BSP_Startup_calibrate(void);
