#include "stdint.h"

extern uint8_t START_BUTTON;
extern uint8_t STOP_BUTTON;
extern uint8_t PDM_comparator;

extern uint8_t pings_ok;

extern uint8_t ping_sm_pedal;
extern uint8_t ping_sm_blackbox;
extern uint8_t ping_sm_battery;
extern uint8_t ping_rpi_comm;



void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void BSP_timer_init(void);
void EXTI15_10_IRQHandler(void);
void BSP_Interrupt_init(void);
void reset_ping_timer(void);
