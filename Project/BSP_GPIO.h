#define ON 1
#define OFF 0


void BSP_GPIO_init(void);
void BSP_GPIO_FANS(uint8_t state);
void BSP_GPIO_RTDS(void);
void BSP_GPIO_brakelight(uint8_t state);
void BSP_GPIO_motor(uint8_t state);
void BSP_GPIO_pilotline(uint8_t state);
